# permutation in string

Given two strings s1 and s2, return true if s2 contains a permutation of s1, or false otherwise.

In other words, return true if one of s1's permutations is the substring of s2.

## Examples

```
Example 1:

Input: s1 = "ab", s2 = "eidbaooo"
Output: true
Explanation: s2 contains one permutation of s1 ("ba").
Example 2:

Input: s1 = "ab", s2 = "eidboaoo"
Output: false
 

Constraints:

1 <= s1.length, s2.length <= 104
s1 and s2 consist of lowercase English letters.
```

## 解析

題目要確認 s2 的子字串包含 s1字串已各種變換字元次序的形式

簡單說就是判斷 s2 具有 s1字串長度相同的子字串 並且組成的字元出現次數相同

所以如果主要就是要找出 s1 字串內所有字元的出現次數

然後透過 slide window 的方式，每次比對與 s1 字元長度相同 slide windows 檢查字元出現次數是否相同

如果全部 slide window 長度接不同則代表沒有

因為所有字元都是小寫 a - z 所以只要用長度 26 的整數陣列 即可儲存所有頻率

先 loop s1 算出所有字元出現次數 在 freq

初始化左界 left 與右界 right 為 0, count = len(s1)

每次檢查 freq [ s[right]- 'a'] 如果大於 1, 更新 count = count - 1

更新 freq [ s[right]- 'a'] -= 1 // 把遇到的字元次數遞減

更新 right += 1

如果 count == 0 代表 已經找到所有出現過的字元 回傳 true

當 right - left >= len(s1)

檢查 freq [ s[left] -'a'] >= 0 如果是 則 更新 count = count + 1

更新 freq [ s[left] -'a'] += 1, left += 1


## 實作

```typescript
export const checkInclusion = (s1: string, s2: string): boolean => {
  const freq: Array<number> = new Array<number>(26);
  freq.fill(0);
  // collect all freq 
  const s1_Len = s1.length;
  const s2_Len = s2.length; 
  const baseIdx = 'a'.charCodeAt(0);
  for (let idx = 0 ; idx < s1_Len; idx++ ) {
      let charIdx =  s1[idx].charCodeAt(0)- baseIdx;
      freq[charIdx]++
  }
  let right = 0;
  let left = 0;
  let count = s1_Len;
  while (right < s2_Len) {
      let rightIdx = s2[right].charCodeAt(0) - baseIdx;
      if ( freq[rightIdx] > 0) {
          count--;
      }
      freq[rightIdx]--;
      right++;
      if (count == 0) {
          return true;
      }
      if (right - left == s1_Len) {
          let leftIdx = s2[left].charCodeAt(0) - baseIdx;
          if (freq[leftIdx] >= 0) {
              count++
          }
          freq[leftIdx]++;
          left++;
      }
  }
  return false;
};
```