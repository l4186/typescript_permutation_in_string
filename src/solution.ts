export const checkInclusion = (s1: string, s2: string): boolean => {
  const freq: Array<number> = new Array<number>(26);
  freq.fill(0);
  // collect all freq 
  const s1_Len = s1.length;
  const s2_Len = s2.length; 
  const baseIdx = 'a'.charCodeAt(0);
  for (let idx = 0 ; idx < s1_Len; idx++ ) {
      let charIdx =  s1[idx].charCodeAt(0)- baseIdx;
      freq[charIdx]++
  }
  let right = 0;
  let left = 0;
  let count = s1_Len;
  while (right < s2_Len) {
      let rightIdx = s2[right].charCodeAt(0) - baseIdx;
      if ( freq[rightIdx] > 0) {
          count--;
      }
      freq[rightIdx]--;
      right++;
      if (count == 0) {
          return true;
      }
      if (right - left == s1_Len) {
          let leftIdx = s2[left].charCodeAt(0) - baseIdx;
          if (freq[leftIdx] >= 0) {
              count++
          }
          freq[leftIdx]++;
          left++;
      }
  }
  return false;
};