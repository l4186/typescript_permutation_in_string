import { checkInclusion } from "./solution";

describe('permutation in string', ()=> {
  it('Example1', () => {
    expect(checkInclusion("ab", "eidbaooo")).toEqual(true)
  });
  it('Example2', () => {
    expect(checkInclusion("ab", "eidboaoo")).toEqual(false)
  });
});